package uk.ac.tees.u0029190.fragmentsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class DynMapActivity extends AppCompatActivity implements SenderFragment.MessageListener{

    private static final String TAG = "DynMapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynmap);

        //Resend fragment
        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", SenderFragment.getServerSummary());
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.dynmapReceiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }
}