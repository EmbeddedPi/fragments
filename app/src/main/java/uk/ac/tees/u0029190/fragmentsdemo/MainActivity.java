package uk.ac.tees.u0029190.fragmentsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements SenderFragment.MessageListener {

    private static final String TAG = "MainActivity";
    public static String EXTRA_MESSAGE = "uk.ac.tees.k0196555.mineCTRL.MainActivity: ";
    private static String mServerSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_main);

        mServerSummary = "ServerName \n IP Address:Port";

        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", mServerSummary);
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    public void onReceiveMessage(String message) {
        Log.d(TAG, "onReceiveMessage: " + message);
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.receiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }

    public void mineTwit(View view) {
        Log.i(TAG, "mineTwit: ");
        Intent intent = new Intent(this, MineTwitActivity.class);
        intent.putExtra(EXTRA_MESSAGE, 0);
        startActivity(intent);
    }

    public void dynMap (View view) {
        Log.i(TAG, "dynMap: ");
        Intent intent = new Intent(this, DynMapActivity.class);
        intent.putExtra(EXTRA_MESSAGE, 1);
        startActivity(intent);
    }
}
