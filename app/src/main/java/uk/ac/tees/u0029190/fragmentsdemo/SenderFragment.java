package uk.ac.tees.u0029190.fragmentsdemo;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SenderFragment.MessageListener} interface
 * to handle interaction events.
 * Use the {@link SenderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SenderFragment extends Fragment {

    private static MessageListener mMessageListener;
    private static String mServerString = "";
    private static final String TAG = "SenderFragment";

    public SenderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SenderFragment.
     */

    public static void setBundle(Fragment fragment, Bundle bundle) {
        fragment.setArguments(bundle);
        mServerString = bundle.getString("summary", "default");
        mMessageListener.onReceiveMessage(mServerString);
    }

    public static String getServerSummary () {
        return mServerString;
    }

    public static SenderFragment newInstance() {
        Log.d(TAG, "newInstance: ");
        SenderFragment fragment = new SenderFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        if (getArguments() != null) {
            Log.d(TAG, "onCreate: Give us an argument then!" + mServerString);
            // No args
        } else {
            Log.d(TAG, "onCreate: No arguments");
        }
    }

    public MessageListener getmMessageListener() {
        Log.d(TAG, "getmMessageListener: ");
        return mMessageListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");

        View view = inflater.inflate(R.layout.fragment_sender, container, false);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        if(context instanceof MessageListener) {
            mMessageListener = (MessageListener)context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MessageListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
        mMessageListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface MessageListener {
        void onReceiveMessage(final String message);
    }
}
