package uk.ac.tees.u0029190.fragmentsdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MineTwitActivity extends AppCompatActivity implements SenderFragment.MessageListener{

    private static final String TAG = "MineTwitActivity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twit);

        SenderFragment senderFragment = SenderFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("summary", SenderFragment.getServerSummary());
        SenderFragment.setBundle(senderFragment, bundle);
    }

    @Override
    public void onReceiveMessage(String message) {
        // Get the receiver fragment
        ReceiverFragment fragment = (ReceiverFragment)getSupportFragmentManager().findFragmentById(R.id.minetwitReceiverFragment);
        if(fragment != null) {
            fragment.updateText(message);
        }
    }
}